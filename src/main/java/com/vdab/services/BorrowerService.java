package com.vdab.services;

import com.vdab.domain.Borrow;
import com.vdab.domain.Borrower;
import com.vdab.repositories.BorrowerRepository;
import lombok.Data;

import java.util.List;

@Data

public class BorrowerService {

    BorrowerRepository borrowerRepository = new BorrowerRepository();

    public Borrower findInfoBorrower() {
        return borrowerRepository.findInfoBorrower();

    }

    public List<Borrower> searchBorrowerByName(String string) {
        return borrowerRepository.searchBorrowerByName(string);
    }


}
