package com.vdab.services;

import com.vdab.domain.Borrow;
import com.vdab.repositories.BorrowRepository;
import lombok.Data;

import java.util.List;

@Data
public class BorrowService {

    BorrowRepository borrowRepository = new BorrowRepository();

    public List<Borrow> showBorrowedGames() {
        return borrowRepository.showBorrowedGames();
    }

    public List<Borrow> listStillBorrowed() {
        return borrowRepository.listStillBorrowed();
    }

//    public List<Borrow> listStillBorrowed() {
//        return borrowRepository.listStillBorrowed();
//    }
}
