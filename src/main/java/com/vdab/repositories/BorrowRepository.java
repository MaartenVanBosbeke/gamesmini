package com.vdab.repositories;

import com.vdab.Main;
import com.vdab.domain.Borrow;
import com.vdab.domain.Borrower;
import com.vdab.domain.Game;
import lombok.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Data

public class BorrowRepository {


    public List<Borrow> showBorrowedGames() {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games","root","P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from game as g inner join difficulty as d on g.difficulty_id = d.id inner join borrow as b on g.id = b.game_id inner join borrower as br on b.borrower_id = br.id order by borrower_name , borrow_date");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            List<Borrow> borrowList = new ArrayList<>();
            while (resultSet.next()){ //zolang er nog resultaten zijn
                Borrow borrow = Borrow.builder()
                        .game   (Game.builder() //make a new game using the builder instead of constructor
                                .gameName(resultSet.getString("game_name"))
                                .build())
                        .borrower(Borrower.builder() //borrower
                                .borrowerName(resultSet.getString("borrower_name"))
                                .build())
                        .borrowDate(resultSet.getDate("borrow_date"))//borrow date
                        .returnDate(resultSet.getDate("return_date")) //return date
                        .build();
                borrowList.add(borrow);
            }
            return borrowList;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public List<Borrow> listStillBorrowed() {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games","root","P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from game as g  inner join borrow as b on g.id = b.game_id inner join borrower as br on b.borrower_id = br.id where return_date is null");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            List<Borrow> borrowList = new ArrayList<>();
            while (resultSet.next()){ //zolang er nog resultaten zijn
                Borrow borrow = Borrow.builder()
                        .game   (Game.builder() //make a new game using the builder instead of constructor
                                .gameName(resultSet.getString("game_name"))
                                .id(resultSet.getInt("id"))
                                .build())
                        .borrower(Borrower.builder() //borrower
                                .borrowerName(resultSet.getString("borrower_name"))
                                .build())
                        .borrowDate(resultSet.getDate("borrow_date"))//borrow date
                        .build();
                borrowList.add(borrow);
            }
            return borrowList;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
