package com.vdab.repositories;

import com.vdab.Main;
import com.vdab.domain.Borrower;
import com.vdab.domain.Category;
import lombok.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Data
public class BorrowerRepository {

    public Borrower findInfoBorrower() {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games","root","P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from borrower where id = 1 ");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            // dit is wat we gaan oproepen via builder
            return Borrower.builder().borrowerName(resultSet.getString("borrower_name")).city(resultSet.getString("city")).build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Borrower> searchBorrowerByName(String string) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games","root","P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from borrower where (borrower_name) LIKE ? ");
            preparedStatement.setString(1, "%" + string + "%");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            List<Borrower> borrowerList = new ArrayList<>();
            while(resultSet.next()) { //zolang er objecten zijn, steek ze in een lijst
                Borrower borrower = Borrower.builder()
                        .borrowerName(resultSet.getString("borrower_name"))
                        .street(resultSet.getString("street"))
                        .houseNumber(resultSet.getString("house_number"))
                        .busNumber(resultSet.getString("bus_number"))
                        .postalCode(resultSet.getString("postcode"))
                        .telephone(resultSet.getString("telephone"))
                        .email(resultSet.getString("email"))
                        .city(resultSet.getString("city"))
                        .id(resultSet.getInt("id")) //extends from baseentity
                        .build();
                borrowerList.add(borrower);
            }
            if(borrowerList.isEmpty()) {
                System.out.println("There is no borrower found with " + string + ", redirecting to main menu");
                Main main = new Main();
                main.showInitialOptions();
            } else {
                return borrowerList; //geen objecten meer, return de list
            }
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
