package com.vdab;

import com.vdab.domain.Borrow;
import com.vdab.domain.Borrower;
import com.vdab.domain.Category;
import com.vdab.domain.Game;
import com.vdab.services.BorrowService;
import com.vdab.services.BorrowerService;
import com.vdab.services.CategoryService;
import com.vdab.services.GameService;


import java.util.*;

public class Main {
    private Scanner scanner = new Scanner(System.in);
    private CategoryService categoryService = new CategoryService();
    private GameService gameService = new GameService();
    private BorrowerService borrowerService = new BorrowerService();
    private BorrowService borrowService = new BorrowService();

    public static void main(String[] args) {

        System.out.println("Welcome to GameStop , designed by Margaux ");
        Main main = new Main();
        main.showInitialOptions();

    }

    public void showInitialOptions() {
        System.out.println("Please select an option: ");
        System.out.println(
                "\t0. Quit the game. \n" +
                        "\t1. Show category for id. \n" +
                        "\t2. Show the fifth game. \n" +
                        "\t3. Show the first borrower. \n" +
                        "\t4. Show a game of your choice. \n" +
                        "\t5. Show all games. \n" +
                        "\t6. Show a list of games and choose a game. \n" +
                        "\t7. Show borrowed games. \n" +
                        "\t8. Advanced search : difficulty. \n" +
                        "\t9. Complex search: borrowers. \n" +
                        "\t10. New way. \n" +
                        "\t11. Show all borrows , and insert a new borrow. \n" +
                        "\t12. Show all borrows from last month and update a borrower id \n" +
                        "\t---------------------------\n" +
                        "\tThat were all choose options. Choose your options:");

        String choice = scanner.next();
        switch (choice) {
            case "0":
                System.out.println("Exiting");
                System.exit(0);
            case "1":
                findCategoryOne();
                showInitialOptions();
                break;
            case "2":
                findFifthGame();
                break;
            case "3":
                findInfoBorrower();
                break;
            case "4":
                findGameByPartName();
                break;
            case "5":
                showAllGames();
                break;
            case "6":
                showAndChoose();
                break;
            case "7":
                showBorrowedGames();
                break;
            case "8":
                searchByDifficulty();
                break;
            case "9":
                searchBorrowerByName();
                break;
            case "10":
                listStillBorrowed();
                break;
            case "11":
                registerNewLoans();
                break;
            default:
                System.out.println("show initial options - default - Wrong input");
                showInitialOptions();
                break;
        }
    }

    private void registerNewLoans() {
        System.out.println("All games: ");
        List<Game> gameList = gameService.showAllGamesById();
        for(Game g : gameList){
            System.out.println(g.getId() + " " + g.getGameName());
        }

        System.out.println("Please enter ID of the game you want to borrow");
        int choice = scanner.nextInt();
        List<Borrow> borrowList = borrowService.listStillBorrowed();
        for(Borrow b : borrowList){
            if(choice == b.getGame().getId()){
                System.out.println("Game is already lent out");
                showInitialOptions();
            }
        }
        Game game = gameService.findById(choice);
        System.out.println(game.toString());
        System.out.println("--------------");
        System.out.println();




        showInitialOptions();
    }

    private void listStillBorrowed() {
        List<Borrow> borrowList = borrowService.listStillBorrowed();
        for(Borrow b : borrowList){
            System.out.println(b.getGame().getGameName() + " " + b.getBorrower().getBorrowerName() + " " + b.getBorrowDate());
        }
        System.out.println();
        showInitialOptions();
    }


    private void searchByDifficulty() {
        System.out.println("Please select a difficulty" +
                "\n1. Very easy" +
                "\n2. Easy" +
                "\n3. Average" +
                "\n4. Difficult" +
                "\n5. Very difficult");
        int choice = scanner.nextInt();
        if (choice > 0 && choice <= 5) {
            List<Game> gameList = gameService.searchByDifficulty(choice);
            for (Game g : gameList) {
                System.out.println(g.getGameName() + " " + g.getEditor() +
                        " " + g.getPrice() + " " + g.getDifficulty().getDifficultyName());
                System.out.println("-----------------");
            }
        } else {
            System.out.println("Wrong input, try again");
            searchByDifficulty();
        }
        showInitialOptions();
    }

    public void showBorrowedGames() {
        List<Borrow> borrowList = borrowService.showBorrowedGames();
        for (Borrow borrow : borrowList) {
            System.out.println(borrow.getGame().getGameName() + "\t" + borrow.getBorrower().getBorrowerName()
                    + "\t" + borrow.getBorrowDate() + "\t" + borrow.getReturnDate());
        }
        System.out.println("Do you want to search for a borrower by name?" +
                "\n1. Search by name" +
                "\n2. Quit");
        int choice = scanner.nextInt();
        if (choice == 1) {
            searchBorrowerByName();
        } else {
            showInitialOptions();
        }
    }

    private void searchBorrowerByName() {
        System.out.println("Type in the name of the borrower you want to search for");
        List<Borrower> borrowerList = borrowerService.searchBorrowerByName(scanner.next());
        for (Borrower b : borrowerList) {
            System.out.println(b.getBorrowerName() + " " + b.getCity() + " " + b.getTelephone() + " " + b.getEmail());
        }
        System.out.println("-----------");
        showInitialOptions();
    }

    private void findCategoryOne() {
        Category category = categoryService.findCategoryOne();
        System.out.println("the category and the id are : " + category.getCategoryName() + " " + category.getId());
        System.out.println("-----------------------------------------------------------------");
    }

    private void findFifthGame() {
        Game game = gameService.findFifthGame();
        System.out.println("printing out game 5 " + game.toString());
        System.out.println("-----------------------------------------------");
        showInitialOptions();
    }

    private void findInfoBorrower() {
        Borrower borrower = borrowerService.findInfoBorrower();
        System.out.println("We are printing out the name and the city of the borrower : " + borrower.getBorrowerName() + " " + borrower.getCity());
        System.out.println("-----------------------------------");
    }

    public void findGameByPartName() {
        System.out.println("Enter part of the name or part of the name of the game ");
        Game game = gameService.findGameByPartName(scanner.next().toLowerCase(Locale.ROOT));
        System.out.println(game.getGameName() + " " + game.getAge() + " " + game.getAuthor() + " " + game.getPrice());
        System.out.println("------------------------------------------------");
    }

    private void showAllGames() {
        List<Game> gameList = gameService.showAllGames();
        for (Game g : gameList) {
            System.out.println(g.getGameName() + " " + g.getEditor() + " " + g.getPrice());
            System.out.println("---------------------------------------");
        }
    }

    private void showAndChoose() {
        showAllGames();
        System.out.println("Enter the name of the game you want to find");
        Game game = gameService.showAndChoose(scanner.next().toLowerCase(Locale.ROOT));
        System.out.println(game.getGameName() + " " + game.getDifficulty().getDifficultyName() + " " + game.getCategory().getCategoryName());
        System.out.println("--------------------------------");
    }


}
